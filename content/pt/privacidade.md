---
title: Política de privacidade
description: Privavidade no Centro Linux
featured_image: ''
omit_header_text: false
type: page
menu: main
---

# Privacidade no site do Centro Linux

O site do Centro Linux em si não recolhe quaisquer dados sobre os utilizadores/visitantes. Contudo o site é alojado no GitLab (usando gitlab pages) e por isso recomendamos a leitura da [Declaração de Privacidade do GitLab](https://about.gitlab.com/privacy/).

# Nas inscrições para os eventos do Centro Linux

Actualmente o Centro Linux está alojado no Makers In Little Lisbon - MILL, e utiliza a infraestrutura online do MILL para processar as inscrições nos eventos.

Os dados recolhidos neste âmbito são utilizados exclusivamente para o fim de controlar as inscrições.

Não são recolhidos quaisquer dados sobre os acompanhantes.

Os dados recolhidos são apagados regularmente, sendo tentamos evitar que sejam mantidos mais tempo do que necessitamos deles. Sendo que procedimentalmente apagamos no máximo 6 meses depois da realização do evento.

## Para o que utilizamos os dados recolhidos nas inscrições?

Os dados recolhidos durante a inscrição em um evento/actividade do Centro Linux são usados exclusivamente para

1. comunicação da aceitação/rejeição da inscrição como uma, ou mais vagas preenchidas
2. comunicação de alterações à agenda do evento, como alterações de horário, ou adiamentos para datas posteriores
3. resposta a pedidos de utilização de equipamentos do Centro Linux durante os eventos/actividades
4. resposta a pedido esclarecimento de dúvidas colocadas por quem se propõem a ter uma ou mais vagas preenchidas, ou outros tipos de respostas a questões que sejam colocadas ao Centro Linux
5. comunicação de cancelamento de eventos aos inscritos

## Inscrição de menores

A inscrição e participação de menores nas actividades do Centro Linux, tem que ser sempre feita por um adulto responsável pelo menor (por exemplo um dos seus pais), e a inscrição deve ser sempre feita em nome do adulto, para evitarmos o processamento dos dados pessoais.

# Privacidade nos eventos do Centro Linux

Durante o evento, não precisamos nem recolhemos de quaisquer dados pessoais para lá do nome do inscrito para o fim exclusivo da validação da inscrição.

Os participantes e voluntários são livres de utilizar pseudónimos durante o evento.

É contra as regras do Centro Linux tirar fotografias a indivíuos sem sua autorização prévia.

É contra as regras do Centro Linux publicar fotografias a indivíuos sem sua autorização prévia.

Todos os voluntários do Centro Linux serão informados sobre regras e comportamentos recomendados durante a realização dos eventos.

Os voluntários do Centro Linux procuram notificar os participantes sobre as regras e comportamentos recomendados durante a realização dos eventos.

## Participação de menores

Durante a realização dos eventos a protecção dos dados dos menores é da responsabilidade do adulto acompanhante, dos mesmos onde eles tiverem legalmente e/ou parentalmente reconhecida autonomia. Mas os voluntários do Centro Linux contribuirão para o exercício da protecção dos dados pessoais dos menores de acordo com a Lei, com as regras do Centro Linux, do MILL e indicações do adulto responsável.

# Contactos para fins de privacidade

Para fim de exercício de direitos relativos aos vossos dados pessoais, podem contactar-nos por correio electrónico, ou pessoalmente (durante os eventos do Centro Linux).

O endereço de correio electrónico a usar é o: privacidade {at} centrolinux ponto pt

# O Centro Linux
É uma iniciativa comunitária orgânica e não uma pessoa singular, ou colectiva legalmente constituida.