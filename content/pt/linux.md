---
title: Linux
description: Actividades sobre Linux ;)
featured_image: ''
omit_header_text: false
type: page
menu: footer
---

# Actividades do Centro Linux dedicadas ao Linux

Eventos no Centro Linux dedicados ao Linux!? Que ideia mais louca!

## Os próximos eventos:

### Ubuntu Mantic Party

Uma festa de lançamento do Ubuntu 23.10 Mantic Minotaur, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt), no próximo dia 28 de Outubro de 2023.

[![Ubuntu Mantic Party](https://centrolinux.pt/images/2023-10.Mantic_Party/cartaz_mantic.png)](https://centrolinux.pt/post/2023-outubro-manticparty/)

### Dia do Debian

Um dia com membros da comunidade Debian em Portugal para conhecermos o Sistema Operativo Universal, no próximo dia 7 de Outubro de 2023.

[![Dia do Debian](https://centrolinux.pt/images/2023.10-Dia_do_Debian/cartaz_debian.png)](https://centrolinux.pt/post/2023-outubro-debian/)


## Eventos anteriores

### Festa Lunar

O evento de Abril de 2023 foi uma festa de lançamento do Ubuntu 23.04 Lunar Lobster, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt).

[![Festa Lunar](https://centrolinux.pt/images/2023.04-Festa_Lunar/post.base.png)](https://centrolinux.pt/post/2023-abril-festa-lunar/)


### A inauguração do Centro Linux

A inauguração do Centro Linux, coincidiu com uma festa de lançamento do Ubuntu 22.10 Kinetic Kudu, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt).

[![A inauguração e festa de lançamento do Kinetic Kudu](https://centrolinux.pt/images/Inauguracao/cartaz_twitter.png)](https://centrolinux.pt/post/2022-dezembro-inauguracao/)
