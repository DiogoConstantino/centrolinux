---
date: 2024-06-16
description: "O Centro Linux no caminho do Home Lab, e encontro de utilizadores de Home Assistant"
featured_image: ""
tags: ["Linux", "2024", "Junho", "ShowAndTell", "Ubuntu-pt", "Hugo", "Ubuntu", "Documentação", "OpenDocumentationAcademy"]
title: "Vem contribuir para o Ubuntu-pt"
---

[![Cartaz](https://centrolinux.pt/images/2024-06.Junho/cartaz.png)](https://osm.org/go/b5crq_xVM?layers=N)


# Actividades:

## "Show and tell" / Mostra e conta

Nesta actividade membros da comunidade que se voluntariem para vir demonstrar, ou partilhar conhecimento têm a oportunidade de ter alguns minutos (5 ou mais dependendo de das inscrições).

As inscrições são feitas no momento.

## Desenvolvimento colectivo do sítio web da Ubuntu-pt

O sítio web da Ubuntu-pt está a ser renovado há algum tempo pela equipa de voluntários para a Web da Ubuntu-pt.

Nesta actividade vamos dar a oportunidade de mais membros da comunidade contribuirem para o desenvolvimento do sítio web.

O novo sítio web da Ubuntu-pt é feito em [Hugo](https://gohugo.io), o que reduz muito o conhecimento técnico necessário para contribuir para o seu desenvolvimento.

Quem tiver interessado em contribuir para o desenvolvimento terá nesta sessão uma oportunidade para coordenação com o resto dos contribuidores e até de aprender o que for necessário para o desenvolvimento do site, incluíndo as bases de criação de sítios web com Hugo.
de

## A Open Documentation Academy

A Open Documentation Academy é uma iniciativa da equipa de Documentação do Ubuntu que visa promover a criação e manutenção de documentação de qualidade para o sistema operativo Ubuntu e para outros projectos de software livre.

Nesta sessão, vamos conhecer a Open Documentation Academy e convidar os participantes a juntarem-se a esta iniciativa. Vamos aprender os objectivos, a estrutura e as formas de contribuir para a Open Documentation Academy.

Esperamos que esta sessão desperte o interesse dos participantes em contribuir para a documentação do Ubuntu e de outros projectos de software livre, ajudando a melhorar a experiência dos utilizadores.



## Quando?

No **Sábado dia 29 de Junho de 2024 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição |
| ----- | --------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Apresentação do [MILL - Makers In Little Lisbon](https://mill.pt) e do Centro Linux |
| 11:45 | "Show and tell"/Mostra e conta |
| 12:30 | Desenvolvimento colectivo do sítio web da Ubuntu-pt |
| 13:00 | Interrupção para o almoço |

##### Tarde
| Hora  | Descrição |
| ----- | --------- |
| 15:00 | Recomeço das actividades |
| 17:00 | Introdução à Open Documentation Academy |
| 19:30 | Desmontagem e fim das actividades |

##### Destalhes das actividades

### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se na [página do evento no site do Makers in Little Lisbon](https://mill.pt/events/vem-contribuir-para-a-ubuntu-pt/)

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
