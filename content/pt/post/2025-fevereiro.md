---
date: 2025-02-10
description: "Firewalls com redundância nas ligações à Internet, e na infraestrutura"
featured_image: ""
tags: ["Router", "Firewall", "2025", "Fevreiro", "Internet", "Meetup", "HomeLab", "Router"]
title: "Firewalls"
---


[![Cartaz](https://centrolinux.pt/images/2025-02.Firewalls/cartaz_fev_2025.png)](https://osm.org/go/b5crq_xVM?layers=N)


Estamos de regresso às nossas actividades regulares no ano de 2025, depois de no passado mês termos estado na inauguração do nosso projecto irmão no Porto, o [ECTL - Porto](https://ectl.pt).

Vamos voltar a focar-nos em construir um ambiente de computação privado, seguro robusto, sempre ligado à Internet.


# Actividades:
Este mês vamos:

1. ter um momento de suporte técnico à comunidade
2. uma surpresa
3. aprender a criar uma firewall com ligações redundantes à Internet.
4. explorar em laboratório a possibilidade de termos uma cloud privada que assume a função de firewall e outros serviços de rede

## Porquê ligações redundantes à Internet?

De forma geral os nossos [fornecedores acesso à Internet ("Internet Service Providers", ou ISP)](https://pt.wikipedia.org/wiki/Fornecedor_de_acesso_%C3%A0_internet), fornecem um serviço que é (em Portugal) tipicamente bastante fiável, mas isso nem sempre acontece, e por vezes alguns de nós queremos garantir na nossa casa, ou no nosso negócio que há sempre um acesso à Internet, mesmo quando o nosso fornecedor de Internet falha.

Com a entrada de um novo ISP, com um serviço de fibra, no mercado português, tornou-se muito acessível à possibilidade de ter mais que um ISP em simultâneo, e essa é uma oportunidade que podemos querer explorar, mas até com fornecedores de LTE e 5G isso é tecnicamente possível.

Se ter Internet é importante para ti, porque precisas mesmo de garantir que a tua empresa não pára nunca, porque precisas mesmo das encomendas que podem chegar a qualquer momento, que no escritório onde trabalhas tens sempre Internet mesmo quando trabalhas em casa, ou até mesmo se precisas de ver vídeos de gatinhos com urgência, então ter uma ligação à Internet redundante é para ti.


## Porquê uma firewall numa cloud privada?

Em primeiro lugar é preciso compreender que neste caso uma cloud privada é baseada numa infraestrutura que fica inteiramente sobre o nosso controlo e na tua casa, ou na tua empresa.

Uma cloud para correr firewall e serviços de rede, assegura a disponibilidade do serviço e capacidade de escalar os recursos da firewall e dos serviços de rede conforme as tuas necessidades a cada momento, até permite criar várias redes segmentadas com várias firewalls diferentes.

É levar a redundância e alta disponibilidade para a tua rede caseira, ou rede do(s) teus escritórios.

Porque precisamos disto? Porque o teu negócio, ou o teu trabalho não podem parar, e as fotos de gatinhos têm que fluir.

### Descrição das actividades

#### Suporte à Comunidade

#### Surpresa!!!

Uma surpresa, é uma surpresa

#### Ligação Redundante à Internet

- Ter um sistema que automaticamente troca da tua Internet que foi a baixo para uma Internet de reserva (Internet de reserva não incluída)? Para:
    - Não ficares mal no "Netflix & Chill";
    - Não passares sem encomendar comida quando tens fome;
    - Não tirares nega no trabalho porque a tua fibra não funciona;
    - Deixar de receber o aumento porque não conseguiste atender a videochamada;
    - Não perderes a "streak" no Wordle;
    - Nunca deixar de puxar pelo mirror do Tomás?

Nesta actividade vamos montar um [pfSense](https://www.pfsense.org/) virtualizado em [Proxmox](https://www.proxmox.com/en/) com acesso a duas ligações à internet (uma fixa e uma em 5G). Vamos tirar partido dos “Gateway groups” para garantir que se o teu acesso primário falha, a tua rede muda para o acesso secundário sem teres que fazer nada.

Quem nos vem explicar tudo isso é o [Rui Pedro Caldeira](https://www.linkedin.com/in/rpcaldeira)
Programador há cerca de 9 anos, com o bichinho do SysAdmin desde sempre.
Há cerca de 5 anos migrou algumas coisas para self-hosting e desde aí nunca mais pensou em voltar atrás.

#### Exploração de uma firewall numa cloud privada

Nesta actividade vamos ser um pouco radicais, vamos explorar a utilização de [Ubuntu](https://ubuntu.com) e [Microcloud](https://microcloud.is), como plataforma de cloud para suportar uma firewall (ou mais) que protege as nossas redes, e que para além de alojar uma firewall, que por estar numa cloud tem a capacidade de se adaptar à carga, também tem alta disponibilidade, e pode fazer mais serviços importantes para estarem disponíveis, incluindo possivelmente bloqueador de trackers, de anúncios, controlos parentais, túneis e redes privadas virtuais, etc...

## Quando?

No **Sábado dia 22 de Fevereiro de 2025 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição |
| ----- | --------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux |
| 11:05 | Montagem do espaço para as actividades |
| 11:30 | Ligação Redundante à Internet |
| 12:15 | **Surpresa** |
| 13:00 | Interrupção para o almoço |

##### Tarde
| Hora  | Descrição |
| ----- | --------- |
| 15:00 | Exploração de uma firewall numa cloud privada |
| 16:30 | Pausa |
| 18:00 | Jogatana de SuperTux Kart |
| 19:00 | Desmontagem e fim das actividades |

##### Detalhes das actividades

### O que precisa?

O Centro Linux irá fornecer o equipamento do seu laboratório para as componentes práticas, mas todos são convidados a trazer um computador portátil, com qualquer sistema operativo idealmente tenha também um software de gestão de máquinas virtuais para participar mais activamente na pesquisa e até na experimentação.

### Local:

As actividades serão realizadas no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.

Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se na [página do evento no site do Makers in Little Lisbon](https://mill.pt/events/14849/)


### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
