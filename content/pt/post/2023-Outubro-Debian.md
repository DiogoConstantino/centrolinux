---
date: 2023-10-07
description: "O sistema operativo Universal"
featured_image: ""
tags: ["2023", "Outubro", "Debian", "Linux"]
title: "Debian"
---

[![Cartaz](https://centrolinux.pt/images/2023.10-Dia_do_Debian/cartaz_debian.png)](https://osm.org/go/b5crq_xVM?layers=N)

# Debian, o sistema operativo universal

O Debian é uma das distribuções de GNU/Linux mais antigas, uma das mais populares e certamente uma das mais influentes social e tecnicamente, sendo também reconheco pela estabilidade.

A dimensão dos seus repositórios, a sua disponibilidade e influência em outras distribuiçẽos e sistemas operativos faz com que seja justamente apelidado como o sistema operativo Universal e com que seja essêncial que os fãs e estudiosos de outros sistemas operativos também estudem o Debian.

Agora com ajuda de membros da comunidade Debian, também vocês podem aprender mais sobre Debian no Centro Linux.

Para nos guiar no nosso primeiro evento dedicado ao Debian, contamos com o [Calhariz](http://blog.calhariz.com/), um destacado membro da comunidade Debian, profissionalmente é um administrador de sistemas, mas é um Debian Developer por amor, começou por utilizar Slackware e depois Debian há mais tempo do que se lembra.

## Quando?

No **Sábado dia 7 de Outubro de 2023 a partir das 11:30**.
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

***Mais detalhes brevemente***

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Introducao ao Debian                                                         |
| 12:30 | Interrupção para o almoço                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 15:00 | Como obter, instalar e manter Debian                                         |
| 17:45 | Pausa                                                                        |
| 18:00 | Como fazer um bom bug report                                                 |
| 19:00 | Fim das actividades, desmontagem e arrumação                                 |
| ----- | ---------------------------------------------------------------------------- |


### Local:

No Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se **[aqui](https://mill.pt/agenda/dia-do-debian/)**, uma pagina no site do Makers in Little Lisbon.

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
