---
date: 2024-04-21
description: "Party like Noble Numbat - a festa de lançamento do Ubuntu 24.04 LTS Noble Numbat no Centro Linux"
featured_image: ""
tags: ["2024", "Abril", "Ubuntu", "Linux", "Noble", "NobleNumbat", "Numbat", "24.04", "ReleaseParty", "InstallParty", "LanParty", "SuperTuxKart" ]
title: "Party like a Noble Numbat"
---

[![Cartaz](https://centrolinux.pt/images/2024-04.Noble_Party/cartaz_noble.png)](https://osm.org/go/b5crq_xVM?layers=N)


# Ubuntu Noble Numbat release party

O [Ubuntu](https://ubuntu.com), é a distribuição de GNU/Linux mais popular, baseado em Debian e com contribuições da comunidade e da Canonical ele é muito estável e amigável a novos utilizadores de GNU/Linux.

Neste evento vamos celebrar mais um lançamento de uma nova versão de Ubuntu, o Ubuntu 24.04 LTS Noble Numbat, e vamos também ter a oportunidade de instalar, e experimentar esta versão da versão de Ubuntu e dos vários sabores criados pela comunidade.


## Quando?

No **Sábado dia 27 de Abril de 2024 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Grupo de Estudo (git)                                               |
| 13:00 | Interrupção para o almoço                                                    |

##### Tarde
| Hora  | Descrição                                                                              |
| ----- | -------------------------------------------------------------------------------------- |
| 15:00 | Sessão de instalação e experimentação do Ubuntu 24.04 LTS Noble Numbat (e seus sabores) |
| 18:00 | Lan Party - SuperTuxKart                                                     |
| 19:00 | Desmontagem e fim das actividades                                            |

##### Destalhes das actividades


###### Sessão de instalação e experimentação do Ubuntu 24.04 LTS Noble Numbat (e seus sabores)

Durante esta sessão vamos ter oportunidade de instalar e experimentar Ubuntu e os seus sabores oficiais nos computadores do laboratório do Centro Linux, também podem trazer os vossos computadores e tentar instalar Ubuntu com ajuda da comunidade (se necessitarem).

Podes trazer o teu computador e experimentar sem instalar, ou instalar, mas se preferires podes fazer o mesmo em um dos computadores do Centro Linux.

###### Lan Party - SuperTuxKart

O [SuperTuxKart](https://supertuxkart.net/pt/Main_Page) é um arcada de corrida de Karts com uma variedade de personagens, pistas e modos para jogar, é muito divertido e não só é multi-plataformas, como é adequado para jogar em família.

Carregeue na imagem para ver o vídeo a partir de demonstração da versão do SuperTuxKart 1.3
[![Vídeo de apresentação do SuperTuxKart versão 1.3](https://trailer.supertuxkart.net/1.3/trailer.jpg)](https://trailer.supertuxkart.net/1.3/trailer.mp4)

**Venha jogar e traga a família!**

### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se na [página do evento no site do Makers in Little Lisbon](https://mill.pt/agenda/party-like-a-noble-numbat/)

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
