---
date: 2023-10-28
description: "Ubuntu Mantic Minotaur release party"
featured_image: ""
tags: ["2023", "Outubro", "Ubuntu", "Linux", "Mantic", "ManticMinotaur", "23.10", "ReleaseParty", "InstallParty", "LanParty", "SuperTuxKart" ]
title: "Ubuntu Mantic Party"
---

[![Cartaz](https://centrolinux.pt/images/2023-10.Mantic_Party/cartaz_mantic.png)](https://osm.org/go/b5crq_xVM?layers=N)


# Ubuntu Mantic Minotaur release party

O [Ubuntu](https://ubuntu.com), é a distribuição de GNU/Linux mais popular, baseado em Debian e com contribuições da comunidade e da Canonical ele é muito estável e amigável a novos utilizadores de GNU/Linux.

Neste evento vamos celebrar mais um lançamento de uma nova versão de Ubuntu, o Ubuntu 23.10 Mantic Minotaur, e vamos também ter a oportunidade de instalar, e experimentar esta versão da versão de Ubuntu e dos vários sabores criados pela comunidade.


## Quando?

No **Sábado dia 28 de Outubro de 2023 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

#### Agenda

##### Manhã

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Hackathon do site Ubuntu-pt.org                                               |
| 13:00 | Interrupção para o almoço                                                    |

##### Tarde
| Hora  | Descrição                                                                              |
| ----- | -------------------------------------------------------------------------------------- |
| 15:00 | Sessão de instalação e experimentação do Ubuntu 23.10 Mantic Minotaur (e seus sabores) |
| 18:00 | Lan Party - SuperTuxKart                                                     |
| 19:00 | Desmontagem e fim das actividades                                            |

##### Destalhes das actividades

###### Hackaton do site ubuntu-pt.org

O site [ubuntu-pt-org](https://ubuntu-pt.org), é o site oficial da Comunidade Local Ubuntu-pt (Comunidade Ubuntu Portugal), e está como podem ver bastante desactualizado.

A Comunidade Ubuntu Portugal quer resolver esse problema, e lançou recentemente um esforço nesse sentido.

Quem pode participar no hackaton?

Todos!
As actividades de pensar/desenhar, e criar um site e seus conteúdos são actividades que diferentes tipos de competências e níveis de conhecimento prévio necessário.

Durante o hackaton a equipa de Web da Comunidade Ubuntu Potugal, irá introduzir todos os participantes a tudo o que precisarem para participarem realizando as actividades que preferirem durante o esforço. E que podem passar por criar código, mas também por escrever textos, fazer gráficos, fazer testes, conceber a estrutura do novo site, arquivar o site actual, etc...

O que posso esperar do Hackaton?

1. aprender a montar um ambiente de desenvolvimento para o site do Ubuntu-pt
2. aprender a contribuir para o site do Ubuntu-pt em diversas funções (desenvolvimento de código, estruturação e escrita de conteúdo, testes, etc...) usando as ferramentas que usamos para a nova versão em desenvolvimento do site.

O que usamos como ferramentas para o site?

Usamos [Hugo](https://gohugo.io/about/what-is-hugo/), um gerador de sites estáticos cujo conteúdo é editar usando [Markdown](https://pt.wikipedia.org/wiki/Markdown), uma linguagem de marcação pequena e muito simples e ainda [GitLab](https://gitlab.com/ubuntu-pt/ubuntu-pt-beta), para alojar o código e alojar o site.

Para o nosso ambiente de contribuição podemos também usar algumas outras ferramentais como [LXD](https://ubuntu.com/lxd), mas se não usa (ainda ;) uma distribuição de GNU/Linux (como o Ubuntu ;) outras ferramentas como o [Multipass](https://multipass.run/) podem ser úteis, se não conhecem estas ou outras ferramentas necessárias e/ou sabem como as utilizar também explicamos e ajudamos com isso.


###### Sessão de instalação e experimentação do Ubuntu 23.10 Mantic Minotaur (e seus sabores)

Durante esta sessão vamos ter oportunidade de instalar e experimentar Ubuntu e os seus sabores oficiais nos computadores do laboratório do Centro Linux, também podem trazer os vossos computadores e tentar instalar Ubuntu com ajuda da comunidade (se necessitarem).



###### Lan Party - SuperTuxKart

O [SuperTuxKart](https://supertuxkart.net/pt/Main_Page) é um arcada de corrida de Karts com uma variedade de personagens, pistas e modos para jogar, é muito divertido e não só é multi-plataformas, como é adequado para jogar em família.

Carregeue na imagem para ver o vídeo a partir de demonstração da versão do SuperTuxKart 1.3
[![Vídeo de apresentação do SuperTuxKart versão 1.3](https://trailer.supertuxkart.net/1.3/trailer.jpg)](https://trailer.supertuxkart.net/1.3/trailer.mp4)

**Venha jogar e traga a família!**

### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se na [página do evento no site do Makers in Little Lisbon](https://mill.pt/agenda/ubuntu-mantic-party/)

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
