---
title: Ubuntu
description: Actividades sobre Ubuntu Centro Linux
featured_image: ''
omit_header_text: false
type: page
menu: footer
---

# Actividades do Centro Linux dedicadas ao Ubuntu

## Ubuntu Mantic Party

Uma festa de lançamento do Ubuntu 23.10 Mantic Minotaur, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt).

[![Ubuntu Mantic Party](https://centrolinux.pt/images/2023-10.Mantic_Party/cartaz_mantic.png)](https://centrolinux.pt/post/2023-outubro-manticparty/)

## Festa Lunar

O evento de Abril de 2023 foi uma festa de lançamento do Ubuntu 23.04 Lunar Lobster, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt).

[![Festa Lunar](https://centrolinux.pt/images/2023.04-Festa_Lunar/post.base.png)](https://centrolinux.pt/post/2023-abril-festa-lunar/)


## A inauguração do Centro Linux

A inauguração do Centro Linux, coincidiu com uma festa de lançamento do Ubuntu 22.10 Kinetic Kudu, organizada pela [Comunidade Local Ubuntu-pt](https://ubuntu-pt).

[![A inauguração e festa de lançamento do Kinetic Kudu](https://centrolinux.pt/images/Inauguracao/cartaz_twitter.png)](https://centrolinux.pt/post/2022-dezembro-inauguracao/)
